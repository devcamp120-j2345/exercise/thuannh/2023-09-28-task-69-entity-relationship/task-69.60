package com.devcamp.relationshipordercustomer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RelationshipOrderCustomerApplication {

	public static void main(String[] args) {
		SpringApplication.run(RelationshipOrderCustomerApplication.class, args);
	}

}
